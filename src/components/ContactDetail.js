import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class ContactDetail extends Component {
    getStyle = () => {
        return {
            background: '#f4f4f4',
            padding: '10px',
            borderBottom: '1px #ccc solid'
        }
    }

    render() {
        if (!this.props.showDetail) {
            return (<div></div>);
        }
        const { id, name, email, phone } = this.props.contact;
        return (
            <div style={this.getStyle()}>
                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                    <div style={{ flex: '10', padding: '5px' }}>Name: {name}</div>
                    <div style={{ flex: '10', padding: '5px' }}>Email: {email}</div>
                    <div style={{ flex: '10', padding: '5px' }}>Phone: {phone}</div>
                    <div style={btnGroup}>
                        <button onClick={this.props.saveContact.bind(this, id)} style={saveBtn}>save</button>
                        <button onClick={this.props.removeContact.bind(this, id)} style={removeBtn}>remove</button>
                    </div>
                </div>
            </div>
        )
    }
}

// PropTypes
ContactDetail.propTypes = {
    saveContact: PropTypes.func.isRequired,
    showDetail: PropTypes.bool.isRequired,
    removeContact: PropTypes.func.isRequired
}

const btnGroup = {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between'
}

const saveBtn = {
    background: '#00ff00',
    fontWeight: 'bold',
    color: '#fff',
    border: 'none',
    padding: '5px 9px',
    cursor: 'pointer'
}
const removeBtn = {
    background: '#ff0000',
    fontWeight: 'bold',
    color: '#fff',
    border: 'none',
    padding: '5px 9px',
    cursor: 'pointer'
}

export default ContactDetail