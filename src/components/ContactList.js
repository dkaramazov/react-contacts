import React, { Component } from 'react';
import Contact from './Contact';
import PropTypes from 'prop-types';

class ContactList extends Component {
  render() {
    return this.props.contacts.map((contact) => (
      <Contact key={contact.id} contact={contact} selectContact={this.props.selectContact} />
    ));
  }
}

// PropTypes
ContactList.propTypes = {
  contacts: PropTypes.array.isRequired,
  selectContact: PropTypes.func.isRequired,
}

export default ContactList;