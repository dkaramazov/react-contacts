import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class AddContact extends Component {
  state = {
    name: '',
    email: '',
    phone: ''
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.props.saveContact(this.state.name, this.state.email, this.state.phone);
    this.setState({ name: '', email: '', phone: '' });
  }

  onChange = (e) => this.setState({ [e.target.name]: e.target.value });

  render() {
    return (
      <form onSubmit={this.onSubmit} style={{ display: 'flex' }}>
        <input
          type="text"
          name="name"
          style={{ flex: '10', padding: '5px' }}
          placeholder="name"
          value={this.state.name}
          onChange={this.onChange}
        />
        <input
          type="text"
          name="email"
          style={{ flex: '10', padding: '5px' }}
          placeholder="email"
          value={this.state.email}
          onChange={this.onChange}
        />
        <input
          type="text"
          name="phone"
          style={{ flex: '10', padding: '5px' }}
          placeholder="phone"
          value={this.state.phone}
          onChange={this.onChange}
        />
        <input
          type="submit"
          value="Submit"
          className="btn"
          style={{ flex: '1' }}
        />
      </form>
    )
  }
}

// PropTypes
AddContact.propTypes = {
  saveContact: PropTypes.func.isRequired
}

export default AddContact
