import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class Contact extends Component {
  getStyle = () => {
    return {
      background: '#f4f4f4',
      padding: '10px',
      borderBottom: '1px #ccc solid'
    }
  }

  render() {
    const { id, name, email } = this.props.contact;
    return (
      <div style={this.getStyle()}>
        <p>
          {name}
          <button onClick={this.props.selectContact.bind(this, id)} style={selectBtn}>view detail</button>
        </p>
      </div>
    )
  }
}

// PropTypes
Contact.propTypes = {
  contact: PropTypes.object.isRequired,
  selectContact: PropTypes.func.isRequired,
}

const removeBtn = {
  background: '#ff0000',
  margin: '0 10px',
  fontWeight: 'bold',
  color: '#fff',
  border: 'none',
  padding: '5px 9px',
  cursor: 'pointer',
  float: 'right'
}
const selectBtn = {
  background: '#0000ff',
  fontWeight: 'bold',
  color: '#fff',
  border: 'none',
  padding: '5px 9px',
  cursor: 'pointer',
  float: 'right'
}

export default Contact
