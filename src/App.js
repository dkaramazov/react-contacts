import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Header from './components/layout/Header';
import ContactList from './components/ContactList';
import ContactDetail from './components/ContactDetail';
import AddContact from './components/AddContact';

import './App.css';

class App extends Component {
  state = {
    contacts: [],
    contact: null,
    showDetail: false
  }

  componentDidMount() {
    // this.setState({ contacts: [] });
    // this.setState({ contact: null });
  }

  removeContact = (id) => {
    this.setState({ contacts: [...this.state.contacts.filter(contact => contact.id !== id)] });
    this.setState({ showDetail: false });
  }

  editContact = (contact) => {
    this.setState({ contact });
  }

  selectContact = (id) => {
    this.setState({ showDetail: true, contact: this.state.contacts.filter(contact => contact.id === id)[0] });
  }

  saveContact = (name, email, phone) => {
    this.setState({ contacts: [...this.state.contacts, { id: Math.round(Math.random() * 10000), name: name, email: email, phone: phone }] });
  }

  render() {
    return (
      <Router>
        <div className="App">
          <Header />
          <div className="container-100">
            <Route exact path="/" render={props => (
              <React.Fragment>
                <AddContact saveContact={this.saveContact} contact={this.state.contact} />
                <div style={{ display: 'flex' }}>
                  <div className="container-50">
                    <ContactList contacts={this.state.contacts} selectContact={this.selectContact} />
                  </div>
                  <div className="container-50">
                    <ContactDetail contact={this.state.contact} saveContact={this.saveContact} showDetail={this.state.showDetail} removeContact={this.removeContact} />
                  </div>
                </div>
              </React.Fragment>
            )} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
